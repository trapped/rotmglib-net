﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using rotmglib.Networking;
using rotmglib.Packets;


namespace rotmglib
{

    public class Client
    {
        #region private vars

        private Connection conn;
        private string host;
        private Int16 port;
        private bool isSending = false;
        private string guid;
        private string password;
        private Int32 gameID;
        private Int32 keyTime;
        private string key;
        private bool delayedLogin;
        private Int32 playerObjectID;
        private Int32 charID;
        private Dictionary<Type, IPacketHandler> packetHandlers;
        private Dictionary<Int32, Object> objects;
        private Object playerObject;
        private PointF currentPosition;
        private Stopwatch tickBase;

        #endregion

        public Client(string server, Int16 port, string guid, string password)
        {
            this.conn = new Connection();
            this.host = server;
            this.port = 2050;

            this.objects = new Dictionary<Int32, Object>();
            this.packetHandlers = new Dictionary<Type, IPacketHandler>();

            this.conn.PacketReceived += handlePacketReceived;
            this.conn.Connected += handleConnected;
            this.conn.ConnectionError += handleConnectionError;
            this.conn.SendCompleted += handleSendCompleted;

            this.guid = guid;
            this.password = password;
        }

        private void handleSendCompleted(object sender, SendCompletedEventArgs e)
        {
            if (sending.Count > 0)
            {
                ClientPacket pkt = sending.Dequeue();
                conn.Send(pkt);
            }
            else
            {
                isSending = false;
            }
        }

        private void handleConnectionError(object sender, ConnectionErrorEventArgs e)
        {
            Console.WriteLine("Connection error: " + e.Error.ToString());
        }

        private void handleConnected(object sender, ConnectedEventArgs e)
        {
            Console.WriteLine("Connected!");
        }

        void Connect(string server, Int16 port)
        {
            if (!conn.IsConnected())
            {
                Reset();
                this.host = server;
                this.port = port;

                conn.Connect(IPAddress.Parse(host), port);
            }
            else
            {
                Console.WriteLine("Already connected!");
            }
        }

        void Disconnect()
        {
            conn.Disconnect();
        }

        void Reset()
        {
            if (conn.IsConnected())
            {
                this.Disconnect();
            }

            this.playerObjectID = -1;
            this.charID = -1;

            this.objects.Clear();
            this.playerObject = null;
            this.currentPosition = new PointF(0.0f, 0.0f);

            this.tickBase = new Stopwatch();
            this.tickBase.Start();
        }

        public T MakePacket<T>(RawPacket pkt) where T : ServerPacket, new()
        {
            MemoryStream mem = new MemoryStream(pkt.GetPayload());

            T result = new T();
            result.Deserialize(new DReader(mem));

            return result;
        }

        private Queue<ClientPacket> sending = new Queue<ClientPacket>();

        private void SendPacket(ClientPacket pkt)
        {
            if (isSending)
            {
                sending.Enqueue(pkt);
            }
            else
            {
                isSending = true;
                conn.Send(pkt);
            }
        }

        public void Login(Int32 charID)
        {
            Login(this.host, this.port, charID, -2, -1, "");
        }

        public void Login(string server, Int16 port, Int32 charID, Int32 gameID, Int32 keyTime, string key)
        {
            if (!conn.IsConnected())
            {
                Connect(server, port);
                this.charID = charID;
                SendHello(gameID, keyTime, key);
            }
            else
            {
                this.charID = charID;
                this.gameID = gameID;
                this.keyTime = keyTime;
                this.key = key;
                this.delayedLogin = true;
            }
        }

        public void Logout()
        {
            Disconnect();
        }

        public void Load(Int32 charID)
        {
            SendLoad(charID);
        }

        public void Move(PointF position)
        {
            this.currentPosition = position;
        }

        public void UsePortal(Int32 objectID)
        {
            SendUsePortal(objectID);
        }

        public void InventorySwap(Slot from, Slot to)
        {
            SendInventorySwap(GetTick(), from, to);
        }

        public Dictionary<Int32, Object> GetObjects()
        {
            return this.objects;
        }

        public Object GetObject(Int32 id)
        {
            Object returns = null;
            objects.TryGetValue(id, out returns);
            return returns;
        }

        public Object GetPlayerObject()
        {
            return this.playerObject;
        }

        protected void SendHello(Int32 gameID, Int32 keyTime, string key)
        {
            Hello pkt = new Hello(
                this.guid,
                this.password,
                gameID,
                keyTime,
                key
            );
            SendPacket(pkt);
        }

        protected void SendLoad(Int32 charID)
        {
            Packets.Load pkt = new Load(charID, false);
            SendPacket(pkt);
        }

        protected void SendUpdateAck()
        {
            UpdateAck pkt = new UpdateAck();
            SendPacket(pkt);
        }

        protected void SendMove(Int32 tickID, Int32 tickTime, PointF position, TimedPoint[] records)
        {
            Packets.Move pkt = new Move(tickID, tickTime, position, records);
            SendPacket(pkt);
        }

        protected void SendPong(Int32 serial, Int32 tickTime)
        {
            Pong pkt = new Pong(serial, tickTime);
            SendPacket(pkt);
        }

        protected void SendGotoAck(Int32 tickTime)
        {
            GotoAck pkt = new GotoAck(tickTime);
            SendPacket(pkt);
        }

        protected void SendShootAck(Int32 tickTime)
        {
            ShootAck pkt = new ShootAck(tickTime);
            SendPacket(pkt);
        }

        protected void SendInventorySwap(Int32 tickTime, Slot from, Slot to)
        {
            Packets.InventorySwap pkt = new InventorySwap(
                tickTime,
                currentPosition,
                from,
                to
            );
            SendPacket(pkt);
        }

        protected void SendUsePortal(Int32 objectID)
        {
            Packets.UsePortal pkt = new UsePortal(objectID);
            SendPacket(pkt);
        }

        private void handleMapInfo(MapInfo pkt)
        {
            //MapInfoCallback cb = new MapInfoCallback(pkt);
            OnMapInfo(pkt);
        }

        private void handleCreateSuccess(CreateSuccess pkt)
        {
            Debug.Assert(this.charID == pkt.CharID);
            this.playerObjectID = pkt.ObjectID;

            //CreateSuccessCallback cb = new CreateSuccessCallback(pkt);
            OnCreateSuccess(pkt);
        }

        private void handleFailure(Failure pkt)
        {
            //FailureCallback cb = new FailureCallback(pkt);
            OnFailure(pkt);
        }

        private void handleReconnect(Reconnect pkt)
        {
            //ReconnectCallback cb = new ReconnectCallback(pkt);
            OnReconnect(pkt);
        }

        private void handleUpdate(Update pkt)
        {
            //Update the object list: dropped objects.
            foreach (var i in pkt.Drops)
            {
                Object obj;
                if (this.objects.TryGetValue(i, out obj))
                {
                    this.objects.Remove(i);
                }
                else
                {
                    throw new Exception("Warning: I have been asked to drop object #" + i + ", but I know no such object.");
                }
            }
            //Update the object list: new objects.
            foreach (Object obj in pkt.NewObjects)
            {
                if (this.objects.ContainsKey(obj.ObjectID))
                {
                    //Object already existed. Should this happen? I will warn and update the object.
                    this.objects[obj.ObjectID] = obj;
                    throw new Exception("Warning: I have been asked to create a new object #" + obj.ObjectID + ", but that object already exists. I will update it.");
                }
                else
                {
                    this.objects.Add(obj.ObjectID, obj);
                }

                if (obj.ObjectID == this.playerObjectID)
                {
                    this.playerObject = obj;
                    this.currentPosition = obj.Position;
                }
            }
            //UpdateCallback cb = new UpdateCallback(pkt);
            OnUpdate(pkt);
        }

        private void handleNewTick(NewTick pkt)
        {
            //Update the object list
            foreach (StatusData i in pkt.Statuses)
            {
                Object obj;
                if (this.objects.TryGetValue(i.ObjectID, out obj))
                {
                    this.objects[i.ObjectID].Position = i.Position;

                    foreach (Stat s in i.Stats)
                        this.objects[i.ObjectID].SetStat(s);

                    if (obj.ObjectID == this.playerObjectID)
                    {
                        this.currentPosition = obj.Position;
                    }
                }
                else
                {
                    Console.WriteLine(
                        "Warning: I have been asked to update object #{0}, but I know no such object.",
                        i.ObjectID
                    );
                }
            }
            //NewTickCallback cb = new NewTickCallback(pkt);
            OnNewTick(pkt);
        }

        private void handlePing(Ping pkt)
        {
            OnPing(pkt);
        }

        private void handleGoto(Goto pkt)
        {
            //TODO: Update position
            OnGoto(pkt);
        }

        private void handleMultiShoot(MultiShoot pkt)
        {
            OnMultiShoot(pkt);
        }

        private void OnMapInfo(MapInfo pkt)
        {
            SendLoad(this.charID);
        }

        private void OnCreateSuccess(CreateSuccess pkt)
        {
            //PostCallback(cb);
        }

        private void OnDisconnected()
        {
            //PostCallback(cb);
        }

        private void OnFailure(Failure pkt)
        {
            //PostCallback(cb);
            Console.WriteLine(pkt.ErrorID + ": " + pkt.ErrorMessage);
        }

        private void OnReconnect(Reconnect pkt)
        {
            //PostCallback(cb);
            //if(!cb.Handled) {
            string host = pkt.Host.Length == 0 ? this.host : pkt.Host;
            Int16 port = pkt.Port == -1 ? (Int16)this.port : pkt.Port;

            Login(host, port, this.charID, pkt.GameID, pkt.KeyTime, pkt.Key);
            //}
        }

        private void OnUpdate(Update pkt)
        {
            //PostCallback(cb);

            //if(!cb.Handled) {
            SendUpdateAck();
            //}
        }

        private void OnNewTick(NewTick pkt)
        {
            //PostCallback(cb);

            //if(!cb.Handled) {
            SendMove(pkt.TickID, GetTick(), currentPosition, null);
            //}
        }

        private void OnPing(Ping pkt)
        {
            SendPong(pkt.Serial, GetTick());
        }

        private void OnGoto(Goto pkt)
        {
            SendGotoAck(GetTick());
        }

        private void OnMultiShoot(MultiShoot pkt)
        {
            SendShootAck(GetTick());
        }

        private Int32 GetTick()
        {
            return (Int32)tickBase.ElapsedMilliseconds;
        }

        private T GetHandler<T>() where T : IPacketHandler, new()
        {
            IPacketHandler result;
            if (packetHandlers.TryGetValue(typeof(T), out result))
            {
                return (T)result;
            }
            else
            {
                result = new T();
                result.Client = this;
                packetHandlers.Add(typeof(T), result);
                return (T)result;
            }
        }

        public void handlePacketReceived(object sender, PacketReceivedEventArgs e)
        {
            switch (e.Packet.PacketID)
            {
                case EPacketID.MAPINFO:
                    handleMapInfo(MakePacket<MapInfo>(e.Packet));
                    break;
                case EPacketID.CREATE_SUCCESS:
                    handleCreateSuccess(MakePacket<CreateSuccess>(e.Packet));
                    break;
                case EPacketID.FAILURE:
                    handleFailure(MakePacket<Failure>(e.Packet));
                    break;
                case EPacketID.UPDATE:
                    handleUpdate(MakePacket<Update>(e.Packet));
                    break;
                case EPacketID.NEW_TICK:
                    handleNewTick(MakePacket<NewTick>(e.Packet));
                    break;
                case EPacketID.RECONNECT:
                    handleReconnect(MakePacket<Reconnect>(e.Packet));
                    break;
                case EPacketID.PING:
                    handlePing(MakePacket<Ping>(e.Packet));
                    break;
                case EPacketID.GOTO:
                    handleGoto(MakePacket<Goto>(e.Packet));
                    break;
                case EPacketID.MULTISHOOT:
                    handleMultiShoot(MakePacket<MultiShoot>(e.Packet));
                    break;
            }
            foreach (var i in packetHandlers)
            {
                i.Value.HandlePacket(e.Packet.PacketID, e.Packet.GetPayload());
            }
        }

        public interface IPacketHandler
        {
            Client Client { get; set; }

            void HandlePacket(EPacketID packetID, byte[] payload);
        }


    }

    abstract class PacketHandlerBase : Client.IPacketHandler
    {
        public abstract void HandlePacket(EPacketID packetID, byte[] payload);

        public Client Client { get; set; }
    }
    //public class MapInfoCallback : CallbackMsg { }
    //public class CreateSuccessCallback : CreateSuccess { }
    //public class FailureCallback : Failure { }
    //public class ReconnectCallback : Reconnect { }
    //public class UpdateCallback : Update { }
    //public class NewTickCallback : NewTick { }

    //public interface ICallback
    //{
    //    void Invoke(ICallbackMsg msg);
    //}
    //class Callback<T> : ICallback where T : class
    //{
    //    Delegate callback;
    //    public Callback(Delegate cb)
    //    {
    //        this.callback = cb;
    //    }
    //}
    //public interface ICallbackMsg
    //{
    //    void Handle(ICallback cb);
    //}
    //class CallbackMsg<T> : ICallbackMsg where T : class
    //{

    //}
}