﻿using System;
using System.IO;

namespace rotmglib
{
    internal class Blob : ISerializable
    {
        public byte[] Data = null;

        public virtual void Serialize(DWriter bw)
        {
            if (Data != null)
            {
                bw.Write((Int32)Data.Length);
                bw.Write(Data);
            }
            else
                bw.Write((Int32)0);

        }

        public virtual void Deserialize(DReader br)
        {
            Int32 len = br.ReadInt32();
            if (len > 0)
            {
                Data = br.ReadBytes(len);
            }
        }
    }
}