﻿using System;
using System.IO;

namespace rotmglib
{
    public class Object : StatusData, ISerializable
    {
        public Int16 Type;

        public override void Serialize(DWriter bw)
        {
            bw.Write(Type);
            base.Serialize(bw);
        }

        public override void Deserialize(DReader br)
        {
            Type = br.ReadInt16();
            base.Deserialize(br);
        }
    }
}