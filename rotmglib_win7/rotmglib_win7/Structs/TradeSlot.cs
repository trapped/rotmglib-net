﻿using System;
using System.IO;

namespace rotmglib
{
    internal class TradeSlot : ISerializable
    {
        public Int32 Item;
        public Int32 SlotTypeID;
        public bool Tradeable;
        public bool Marked;

        void ISerializable.Serialize(DWriter bw)
        {
            bw.Write(Item);
            bw.Write(SlotTypeID);
            bw.Write(Tradeable);
            bw.Write(Marked);
        }

        void ISerializable.Deserialize(DReader br)
        {
            Item = br.ReadInt32();
            SlotTypeID = br.ReadInt32();
            Tradeable = br.ReadBoolean();
            Marked = br.ReadBoolean();
        }
    }
}