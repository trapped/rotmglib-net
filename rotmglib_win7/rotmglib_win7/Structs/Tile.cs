﻿using System.IO;
using System;

namespace rotmglib
{
    public class Tile : ISerializable
    {
        public Int16 X;
        public Int16 Y;
        public Int16 Type;

        public virtual void Serialize(DWriter bw)
        {
            bw.Write(X);
            bw.Write(Y);
            bw.Write(Type);
        }

        public virtual void Deserialize(DReader br)
        {
            X = br.ReadInt16();
            Y = br.ReadInt16();
            Type = br.ReadInt16();
        }
    }
}