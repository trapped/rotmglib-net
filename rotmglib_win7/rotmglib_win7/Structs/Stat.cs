﻿using System;
using System.IO;

namespace rotmglib
{
    public class Stat : ISerializable
    {
        public byte Type;
        public Int32 IntData;
        public string StringData;

        public Stat()
        {
            Type = (byte)0xFF;
            IntData = -1;
            StringData = null;
        }

        public Stat(byte type, Int32 value)
        {
            this.Type = type;
            this.IntData = value;
            this.StringData = null;
        }

        public Stat(byte type, String value)
        {
            this.Type = type;
            this.StringData = value;
            this.IntData = -1;
        }

        public bool IsStringStat()
        {
            switch (Type)
            {
                case (byte)EStatID.NAME:
                case (byte)EStatID.GUILD:
                case (byte)EStatID.ACCOUNT_ID:
                case (byte)EStatID.OWNER_ACCOUNT_ID:
                case (byte)EStatID.UNKNOWN_TEXT_2:
                    return true;

                default:
                    return false;
            }
        }

        public virtual void Serialize(DWriter bw)
        {
            bw.Write(Type);

            if (IsStringStat())
                bw.Write(StringData);
            else
                bw.Write(IntData);
        }

        public virtual void Deserialize(DReader br)
        {
            Type = br.ReadByte();

            if (IsStringStat())
                StringData = br.ReadString();
            else
                IntData = br.ReadInt32();
        }
    }
}