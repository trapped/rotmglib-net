﻿using System.IO;

namespace rotmglib
{
    public interface ISerializable
    {
        void Serialize(DWriter bw);

        void Deserialize(DReader br);
    }
}