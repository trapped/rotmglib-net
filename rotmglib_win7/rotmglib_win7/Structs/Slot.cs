﻿using System;
using System.IO;

namespace rotmglib
{
    public class Slot : ISerializable
    {
        public Int32 ObjectID;
        public byte SlotID;
        public short ObjectType;

        public virtual void Serialize(DWriter bw)
        {
            bw.Write(ObjectID);
            bw.Write(SlotID);
            bw.Write(ObjectType);
        }

        public virtual void Deserialize(DReader br)
        {
            ObjectID = br.ReadInt32();
            SlotID = br.ReadByte();
            ObjectType = br.ReadInt16();
        }
    }
}