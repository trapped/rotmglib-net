﻿using System;
using System.Drawing;
using System.IO;
using System.Collections.Generic;

namespace rotmglib
{
    public class StatusData : ISerializable
    {
        public Int32 ObjectID;
        public PointF Position;
        public List<Stat> Stats;

        public StatusData()
        {
            ObjectID = -1;
            Position = new PointF();
            Stats = new List<Stat>();
        }

        public void SetStat(Stat stat)
        {
            for(int i = 0; i < Stats.Count; i++)
            {
                if (Stats[i].Type == stat.Type)
                {
                    Stats[i] = stat;
                    return;
                }
            }
            Stats.Add(stat);
        }

        public Stat GetStat(byte id)
        {
            foreach(Stat s in Stats)
            {
                if (s.Type == id)
                    return s;
            }
            return null;
        }

        public virtual void Serialize(DWriter bw)
        {
            bw.Write(ObjectID);
            bw.Write(Position);
            bw.Write((Int16)Stats.Count);
        }

        public virtual void Deserialize(DReader br)
        {
            ObjectID = br.ReadInt32();
            Position = br.ReadPoint();
            Stats = br.ReadList<Stat>();
        }
    }
}