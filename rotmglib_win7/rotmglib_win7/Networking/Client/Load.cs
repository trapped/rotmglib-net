﻿using System;
using System.IO;

namespace rotmglib.Packets
{
    internal class Load : ClientPacket
    {
        private Int32 charID;
        private bool fromArena;

        public Load(Int32 CharID, bool FromArena)
        {
            this.charID = CharID;
            this.fromArena = FromArena;
        }

        public override EPacketID PacketID { get { return EPacketID.LOAD; } }

        public override void Serialize(DWriter bw)
        {
            bw.Write(charID);
            bw.Write(fromArena);
        }
    }
}