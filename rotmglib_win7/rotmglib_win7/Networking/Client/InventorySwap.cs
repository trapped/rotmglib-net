﻿using System;
using System.Drawing;
using System.IO;

namespace rotmglib.Packets
{
    internal class InventorySwap : ClientPacket
    {
        private Int32 tickTime;
        private PointF position;
        private Slot from;
        private Slot to;

        public InventorySwap(Int32 TickTime, PointF Position, Slot From, Slot To)
        {
            this.tickTime = TickTime;
            this.position = Position;
            this.from = From;
            this.to = To;
        }

        public override EPacketID PacketID { get { return EPacketID.INVSWAP; } }

        public override void Serialize(DWriter dw)
        {
            dw.Write(tickTime);
            dw.Write(position);
            from.Serialize(dw);
            to.Serialize(dw);
        }
    }
}