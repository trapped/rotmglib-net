﻿using System.IO;

namespace rotmglib.Packets
{
    internal class CancelTrade : ClientPacket
    {
        public override EPacketID PacketID { get { return EPacketID.CANCELTRADE; } }

        public override void Serialize(DWriter dw)
        {
        }
    }
}