﻿using System;
using System.IO;

namespace rotmglib.Packets
{
    internal class UsePortal : ClientPacket
    {
        private Int32 objectID;

        public UsePortal(Int32 ObjectID)
        {
            this.objectID = ObjectID;
        }

        public override EPacketID PacketID { get { return EPacketID.USEPORTAL; } }

        public override void Serialize(DWriter dw)
        {
            dw.Write(objectID);
        }
    }
}