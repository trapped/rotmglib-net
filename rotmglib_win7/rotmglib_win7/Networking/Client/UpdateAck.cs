﻿using System.IO;

namespace rotmglib.Packets
{
    internal class UpdateAck : ClientPacket
    {
        public override EPacketID PacketID { get { return EPacketID.UPDATEACK; } }

        public override void Serialize(DWriter dw) { }
    }
}