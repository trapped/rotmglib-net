﻿using System;
using System.IO;

namespace rotmglib.Packets
{
    internal class ShootAck : ClientPacket
    {
        private Int32 tickTime;

        public ShootAck(Int32 TickTime)
        {
            this.tickTime = TickTime;
        }

        public override EPacketID PacketID { get { return EPacketID.SHOOTACK; } }

        public override void Serialize(DWriter dw)
        {
            dw.Write(tickTime);
        }
    }
}