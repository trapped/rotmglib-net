﻿using System.IO;

namespace rotmglib.Packets
{
    public abstract class ClientPacket : Packet
    {
        abstract public void Serialize(DWriter output);
    }
}