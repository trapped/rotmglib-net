﻿using System.IO;

namespace rotmglib.Packets
{
    internal class ChangeTrade : ClientPacket
    {
        private bool[] marked;

        public ChangeTrade(bool[] Marked)
        {
            marked = Marked;
        }

        public override EPacketID PacketID { get { return EPacketID.CHANGETRADE; } }

        public override void Serialize(DWriter dw)
        {
            dw.Write(marked);
        }
    }
}