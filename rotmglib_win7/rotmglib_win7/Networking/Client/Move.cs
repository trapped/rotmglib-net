﻿using System;
using System.Drawing;
using System.IO;

namespace rotmglib.Packets
{
    internal class Move : ClientPacket
    {
        private Int32 tickID;
        private Int32 tickTime;
        private PointF position;
        private TimedPoint[] records;

        public Move(Int32 TickID, Int32 TickTime, PointF Position, TimedPoint[] Records)
        {
            this.tickID = TickID;
            this.tickTime = TickTime;
            this.position = Position;
            this.records = Records;
        }

        public override EPacketID PacketID { get { return EPacketID.MOVE; } }

        public override void Serialize(DWriter dw)
        {
            dw.Write(tickID);
            dw.Write(tickTime);
            dw.Write(position);
            dw.WriteVector<TimedPoint>(records);
        }
    }

    public struct TimedPoint : ISerializable
    {
        public Int32 Time;
        public PointF Position;

        public TimedPoint(Int32 Time, PointF position)
        {
            this.Time = Time;
            this.Position = position;
        }

        public void Serialize(DWriter bw)
        {
            bw.Write(Time);
            bw.Write(Position);
        }

        public void Deserialize(DReader br)
        {
            Time = br.ReadInt32();
            Position = br.ReadPoint();
        }
    }
}