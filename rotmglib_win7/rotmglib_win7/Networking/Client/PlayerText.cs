﻿using System.IO;

namespace rotmglib.Packets
{
    internal class PlayerText : ClientPacket
    {
        private string text;

        public PlayerText(string Text)
        {
            this.text = Text;
        }

        public override EPacketID PacketID { get { return EPacketID.PLAYERTEXT; } }

        public override void Serialize(DWriter dw)
        {
            dw.Write(text);
        }
    }
}