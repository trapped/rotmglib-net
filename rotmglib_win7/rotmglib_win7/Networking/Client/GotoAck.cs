﻿using System;
using System.IO;

namespace rotmglib.Packets
{
    internal class GotoAck : ClientPacket
    {
        private Int32 tickTime;

        public GotoAck(Int32 TickTime)
        {
            this.tickTime = TickTime;
        }

        public override EPacketID PacketID { get { return EPacketID.GOTOACK; } }

        public override void Serialize(DWriter bw)
        {
            bw.Write(tickTime);
        }
    }
}