﻿using System;
using System.IO;

namespace rotmglib.Packets
{
    internal class Hello : ClientPacket
    {
        public string buildVersion = "15";
        private string guid;
        private string password;
        private string secret;
        private Int32 gameID;
        private Int32 keyTime;
        private string key;
        private string mapInfo;
        private string unknown1;
        private string unknown2;
        private string unknown3;
        private string unknown4;
        private string unknown5;

        public Hello(string GUID, string Password, Int32 GameID, Int32 KeyTime, string Key)
        {
            this.guid = GUID;
            this.password = Password;
            this.secret = "";
            this.gameID = GameID;
            this.keyTime = KeyTime;
            this.key = Key;
            this.mapInfo = "";
            this.unknown1 = "";
            this.unknown2 = "rotmg";
            this.unknown3 = "";
            this.unknown4 = "rotmg";
            this.unknown5 = "";
        }

        public override EPacketID PacketID { get { return EPacketID.HELLO; } }

        public override void Serialize(DWriter bw)
        {
            bw.Write(buildVersion);
            bw.Write(gameID);
            bw.Write(RSA.Encrypt(guid));
            bw.Write(RSA.Encrypt(password));
            bw.Write(RSA.Encrypt(secret));
            bw.Write(keyTime);
            bw.Write(key);
            bw.Write32UTF(mapInfo);
            bw.Write(unknown1);
            bw.Write(unknown2);
            bw.Write(unknown3);
            bw.Write(unknown4);
            bw.Write(unknown5);
        }
    }
}