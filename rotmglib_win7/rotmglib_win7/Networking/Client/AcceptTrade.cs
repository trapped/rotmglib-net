﻿using System.IO;

namespace rotmglib.Packets
{
    internal class AcceptTrade : ClientPacket
    {
        private bool[] myItems;
        private bool[] otherItems;

        public AcceptTrade(bool[] MyItems, bool[] OtherItems)
        {
            myItems = MyItems;
            otherItems = OtherItems;
        }

        public override EPacketID PacketID { get { return EPacketID.ACCEPTTRADE; } }

        public override void Serialize(DWriter bw)
        {
            bw.Write(myItems);
            bw.Write(otherItems);
        }
    }
}