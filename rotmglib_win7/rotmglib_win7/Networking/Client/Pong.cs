﻿using System;
using System.IO;

namespace rotmglib.Packets
{
    internal class Pong : ClientPacket
    {
        private Int32 serial;
        private Int32 tickTime;

        public Pong(Int32 Serial, Int32 TickTime)
        {
            this.serial = Serial;
            this.tickTime = TickTime;
        }

        public override EPacketID PacketID { get { return EPacketID.PONG; } }

        public override void Serialize(DWriter dw)
        {
            dw.Write(serial);
            dw.Write(tickTime);
        }
    }
}