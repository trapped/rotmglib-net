﻿using System.IO;

namespace rotmglib.Packets
{
    internal class RequestTrade : ClientPacket
    {
        private string player;

        public RequestTrade(string Player)
        {
            this.player = Player;
        }

        public override EPacketID PacketID { get { return EPacketID.REQUESTTRADE; } }

        public override void Serialize(DWriter dw)
        {
            dw.Write(player);
        }
    }
}