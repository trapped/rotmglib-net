﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Encodings;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.OpenSsl;
using System;
using System.IO;
using System.Text;

namespace rotmglib
{
    internal static class RSA
    {
        private readonly static string KKEY = @"
-----BEGIN RSA PUBLIC KEY-----
MIGJAoGBAMSaASZByAt3Sa9aovoAqaHOMl9mgCZkLQ9SYbwr0IHUyXrw/TRLd5T5
NIvlobwyDdOKm+zTaALa94LXSSdGLusSxTAvz6HbYSyOekpn/srlhX1x5+vJMydL
THhAZltKR0/P+suHS4bCTxm7RwQpvFDK93f6lKapnn9lAZoWOb1NAgMBAAE=
-----END RSA PUBLIC KEY-----"; //INVALID

        private static RsaEngine engine;
        private static AsymmetricKeyParameter key;

        static RSA()
        {
            key = (new PemReader(new StringReader(KKEY.Trim())).ReadObject() as AsymmetricKeyParameter);
            engine = new RsaEngine();
            engine.Init(true, key);
        }

        public static string Decrypt(string str)
        {
            if (string.IsNullOrEmpty(str)) return "";
            byte[] dat = Convert.FromBase64String(str);
            var encoding = new Pkcs1Encoding(engine);
            encoding.Init(false, key);
            return Encoding.UTF8.GetString(encoding.ProcessBlock(dat, 0, dat.Length));
        }

        public static string Encrypt(string str)
        {
            if (string.IsNullOrEmpty(str)) return "";
            byte[] dat = Encoding.UTF8.GetBytes(str);
            var encoding = new Pkcs1Encoding(engine);
            encoding.Init(true, key);
            return Convert.ToBase64String(encoding.ProcessBlock(dat, 0, dat.Length));
        }
    }
}