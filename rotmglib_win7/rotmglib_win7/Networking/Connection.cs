﻿using System;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.IO;
using rotmglib.Packets;


namespace rotmglib.Networking
{

    internal class Connection
    {
        private Socket ConnectionSocket;
        private int readOffset;
        private int readLeft;
        public static RC4 cli2svr = new RC4(new byte[] { 0x31, 0x1f, 0x80, 0x69, 0x14, 0x51, 0xc7, 0x1b, 0x09, 0xa1, 0x3a, 0x2a, 0x6e });
        public static RC4 svr2cli = new RC4(new byte[] { 0x72, 0xc5, 0x58, 0x3c, 0xaf, 0xb6, 0x81, 0x89, 0x95, 0xcb, 0xd7, 0x4b, 0x80 });

        public event EventHandler<PacketReceivedEventArgs> PacketReceived;
        public event EventHandler<ConnectionErrorEventArgs> ConnectionError;
        public event EventHandler<ConnectedEventArgs> Connected;
        public event EventHandler<SendCompletedEventArgs> SendCompleted;

        public Connection()
        {
            this.ConnectionSocket = new Socket(
                AddressFamily.InterNetwork,
                SocketType.Stream,
                ProtocolType.Tcp
            );
            this.readOffset = 0;
            this.readLeft = 0;
        }

        public void Disconnect()
        {
            ConnectionSocket.Disconnect(true);
        }

        public bool IsConnected()
        {
            return this.ConnectionSocket.Connected;
        }

        public void Send(ClientPacket pkt)
        {
            MemoryStream mem = new MemoryStream();
            pkt.Serialize(new DWriter(mem));
            byte[] payload = cli2svr.Crypt(mem.ToArray());

            mem = new MemoryStream();
            DWriter dw = new DWriter(mem);

            dw.Write((Int32)payload.Length + 5);
            dw.Write((byte)pkt.PacketID);
            dw.Write(payload);

            payload = mem.ToArray();
            SocketAsyncEventArgs e = new SocketAsyncEventArgs();
            e.SetBuffer(payload, 0, payload.Length);
            e.Completed += handleSendCompleted;

            if (ConnectionSocket.SendAsync(e) == false)
            {
                handleSendCompleted(ConnectionSocket, e);
            }
        }

        private void beginReceive()
        {
            SocketAsyncEventArgs e = new SocketAsyncEventArgs();
            byte[] buffer = new byte[10240];
            e.SetBuffer(buffer, 0, buffer.Length);
            e.Completed += handleReceiveCompleted;
            if (ConnectionSocket.ReceiveAsync(e) == false)
            {
                handleReceiveCompleted(ConnectionSocket, e);
            }
        }

        private void handleReceiveCompleted(object sender, SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {
                //int available = readLeft + e.BytesTransferred;
                int left = readLeft + e.BytesTransferred;
                int offset = readOffset;

                MemoryStream mem = new MemoryStream(
                    e.Buffer,
                    offset,
                    left
                );
                DReader dr = new DReader(mem);

                while (left >= 5)
                {
                    Int32 len = dr.ReadInt32();
                    byte type = dr.ReadByte();

                    if (left < len)
                    {
                        if (offset + left == e.Buffer.Length)
                        {
                            if (offset > left)
                            {
                                // Dont resize the buffer, instead make place and read again
                                Buffer.BlockCopy(
                                    e.Buffer,
                                    offset,
                                    e.Buffer,
                                    0,
                                    left
                                );
                                e.SetBuffer(left, e.Buffer.Length - left);

                                readOffset = 0;
                                readLeft = left;
                            }
                            else
                            {
                                byte[] newbuf = new byte[e.Buffer.Length * 2];
                                Buffer.BlockCopy(
                                    e.Buffer,
                                    offset,
                                    newbuf,
                                    0,
                                    left
                                );
                                e.SetBuffer(newbuf, left, newbuf.Length - left);

                                readOffset = 0;
                                readLeft = left;
                            }
                        }
                        else
                        {
                            e.SetBuffer(
                                offset + left,
                                e.Buffer.Length - offset - left
                            );

                            readOffset = offset;
                            readLeft = left;
                        }

                        if (ConnectionSocket.ReceiveAsync(e) == false)
                        {
                            handleReceiveCompleted(ConnectionSocket, e);
                        }
                        return;
                    }

                    RawPacket pkt = new RawPacket(
                        (EPacketID)type,
                        svr2cli.Crypt(dr.ReadBytes(len - 5))
                    );
                    PacketReceivedEventArgs ef = new PacketReceivedEventArgs();
                    ef.Packet = pkt;
                    PacketReceived(this, ef);

                    left -= len;
                    offset += len;
                }

                Buffer.BlockCopy(e.Buffer, offset, e.Buffer, 0, left);
                e.SetBuffer(left, e.Buffer.Length - left);

                readOffset = 0;
                readLeft = left;

                if (ConnectionSocket.ReceiveAsync(e) == false)
                {
                    handleReceiveCompleted(ConnectionSocket, e);
                }
            }
            else
            {
                Disconnect();
                ConnectionErrorEventArgs ee = new ConnectionErrorEventArgs();
                ee.Error = e.SocketError;
                ConnectionError(this, ee);
            }
        }

        private void handleSendCompleted(object sender, SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {
                if ((e.Offset + e.BytesTransferred) < e.Buffer.Length)
                {
                    e.SetBuffer(
                        e.Offset + e.BytesTransferred,
                        e.Buffer.Length - e.BytesTransferred - e.Offset
                    );
                    if (ConnectionSocket.SendAsync(e) == false)
                    {
                        handleSendCompleted(ConnectionSocket, e);
                    }
                    return;
                }

                SendCompletedEventArgs ee = new SendCompletedEventArgs();
                SendCompleted(this, ee);
            }
            else
            {
                Disconnect();
                ConnectionErrorEventArgs ee = new ConnectionErrorEventArgs();
                ee.Error = e.SocketError;
                ConnectionError(this, ee);
            }
        }

        public void Connect(IPAddress server, int port)
        {
            SocketAsyncEventArgs e = new SocketAsyncEventArgs();
            e.Completed += handleConnected;
            e.RemoteEndPoint = new IPEndPoint(server, port);
            if (ConnectionSocket.ConnectAsync(e) == false)
            {
                if (e.SocketError == SocketError.Success)
                {
                    handleConnected(ConnectionSocket, e);
                }
                else
                {
                    handleConnectionError(ConnectionSocket, e);
                }
            }
        }

        private void handleConnectionError(object ConnectionSocket, SocketAsyncEventArgs e)
        {
            ConnectionErrorEventArgs ee = new ConnectionErrorEventArgs();
            ee.Error = e.SocketError;
            ConnectionError(this, ee);
        }

        private void handleConnected(object sender, SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.Success)
            {
                ConnectedEventArgs ee = new ConnectedEventArgs();
                Connected(this, ee);
                beginReceive();
            }
            else
            {
                ConnectionErrorEventArgs ee = new ConnectionErrorEventArgs();
                ee.Error = e.SocketError;
                ConnectionError(this, ee);
            }
        }


    }

    public class PacketReceivedEventArgs : EventArgs
    {
        public RawPacket Packet { get; set; }
    }

    public class ConnectionErrorEventArgs : EventArgs
    {
        public SocketError Error { get; set; }
    }

    public class ConnectedEventArgs : EventArgs
    {
    }

    public class SendCompletedEventArgs : EventArgs
    {

    }
}