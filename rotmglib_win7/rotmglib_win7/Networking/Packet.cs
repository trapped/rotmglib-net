﻿namespace rotmglib
{
    public abstract class Packet
    {
        abstract public EPacketID PacketID { get; }
    }
}