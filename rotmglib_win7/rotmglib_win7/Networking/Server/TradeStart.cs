﻿using System.IO;

namespace rotmglib.Packets
{
    internal class TradeStart : ServerPacket
    {
        public TradeSlot[] MyItems;
        public string Player;
        public TradeSlot[] OtherItems;

        public override EPacketID PacketID { get { return EPacketID.TRADESTART; } }

        public override void Deserialize(DReader dr)
        {
            MyItems = dr.ReadVector<TradeSlot>();
            Player = dr.ReadString();
            OtherItems = dr.ReadVector<TradeSlot>();
        }
    }
}