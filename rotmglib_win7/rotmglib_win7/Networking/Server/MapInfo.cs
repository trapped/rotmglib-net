﻿using System;
using System.IO;

namespace rotmglib.Packets
{
    internal class MapInfo : ServerPacket
    {
        public Int32 Width;
        public Int32 Height;
        public string Name;
        public string Unknown1;
        public UInt32 Seed;
        public Int32 Background;
        public Int32 Unknown2;
        public bool AllowPlayerTeleport;
        public bool ShowDisplays;
        public Blob[] ClientXML;
        public Blob[] ExtraXML;

        public override EPacketID PacketID { get { return EPacketID.MAPINFO; } }

        public override void Deserialize(DReader dr)
        {
            Width = dr.ReadInt32();
            Height = dr.ReadInt32();
            Name = dr.ReadString();
            Unknown1 = dr.ReadString();
            Seed = dr.ReadUInt32();
            Background = dr.ReadInt32();
            Unknown2 = dr.ReadInt32();
            AllowPlayerTeleport = dr.ReadBoolean();
            ShowDisplays = dr.ReadBoolean();
            ClientXML = dr.ReadVector<Blob>();
            ExtraXML = dr.ReadVector<Blob>();
        }
    }
}