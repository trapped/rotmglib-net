﻿using System;
using System.IO;

namespace rotmglib.Packets
{
    internal class NewTick : ServerPacket
    {
        public Int32 TickID;
        public Int32 TickTime;
        public StatusData[] Statuses;

        public override EPacketID PacketID { get { return EPacketID.NEW_TICK; } }

        public override void Deserialize(DReader dr)
        {
            TickID = dr.ReadInt32();
            TickTime = dr.ReadInt32();
            Statuses = dr.ReadVector<StatusData>();
        }
    }
}