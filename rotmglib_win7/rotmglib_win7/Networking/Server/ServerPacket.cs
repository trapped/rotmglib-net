﻿using System.IO;

namespace rotmglib.Packets
{
    public abstract class ServerPacket : Packet
    {
        abstract public void Deserialize(DReader input);
    }
}