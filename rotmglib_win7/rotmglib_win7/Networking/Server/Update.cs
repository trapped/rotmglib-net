﻿using System;
using System.Collections.Generic;
using System.IO;

namespace rotmglib.Packets
{
    internal class Update : ServerPacket
    {
        public Tile[] Tiles;
        public Object[] NewObjects;
        public Int32[] Drops;

        public override EPacketID PacketID { get { return EPacketID.UPDATE; } }

        public override void Deserialize(DReader dr)
        {
            Tiles = dr.ReadVector<Tile>();
            NewObjects = dr.ReadVector<Object>();

            List<int> data = new List<int>();
            int len = dr.ReadInt16();
            for (var i = 0; i < len; i++)
            {
                data.Add(dr.ReadInt32());
            }

            Drops = data.ToArray();
        }
    }
}