﻿using System;
using System.IO;

namespace rotmglib.Packets
{
    internal class CreateSuccess : ServerPacket
    {
        public Int32 ObjectID;
        public Int32 CharID;

        public override EPacketID PacketID { get { return EPacketID.CREATE_SUCCESS; } }

        public override void Deserialize(DReader dr)
        {
            ObjectID = dr.ReadInt32();
            CharID = dr.ReadInt32();
        }
    }
}