﻿using System.IO;

namespace rotmglib.Packets
{
    internal class TradeRequested : ServerPacket
    {
        public string Player;

        public override EPacketID PacketID { get { return EPacketID.TRADEREQUESTED; } }

        public override void Deserialize(DReader dr)
        {
            Player = dr.ReadString();
        }
    }
}