﻿using System.IO;

namespace rotmglib.Packets
{
    internal class TradeAccepted : ServerPacket
    {
        public TradeSlot[] MyOffer;
        public TradeSlot[] OtherOffer;

        public override EPacketID PacketID { get { return EPacketID.TRADEACCEPTED; } }

        public override void Deserialize(DReader dr)
        {
            MyOffer = dr.ReadVector<TradeSlot>();
            OtherOffer = dr.ReadVector<TradeSlot>();
        }
    }
}