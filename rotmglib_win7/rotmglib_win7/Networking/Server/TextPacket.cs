﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace rotmglib.Packets
{
    class TextPacket : ServerPacket
    {
        public string Sender;
        public Int32 ObjectID;
        public Int32 Stars;
        public byte BubbleTime;
        public string Recipient;
        public string Text;
        public string CleanText;

        public override EPacketID PacketID { get { return EPacketID.TEXT; } }

        public override void Deserialize(DReader dr)
        {
            Sender = dr.ReadString();
            ObjectID = dr.ReadInt32();
            Stars = dr.ReadInt32();
            BubbleTime = dr.ReadByte();
            Recipient = dr.ReadString();
            Text = dr.ReadString();
            CleanText = dr.ReadString();
        }
    }
}
