﻿using System;
using System.IO;

namespace rotmglib.Packets
{
    internal class Reconnect : ServerPacket
    {
        public string Name;
        public string Host;
        public Int16 Port;
        public Int32 GameID;
        public Int32 KeyTime;
        public bool IsFromArena;
        public string Key;

        public override EPacketID PacketID { get { return EPacketID.RECONNECT; } }

        public override void Deserialize(DReader dr)
        {
            Name = dr.ReadString();
            Host = dr.ReadString();
            Port = dr.ReadInt16();
            GameID = dr.ReadInt32();
            KeyTime = dr.ReadInt32();
            IsFromArena = dr.ReadBoolean();
            Key = dr.ReadString();
        }
    }
}