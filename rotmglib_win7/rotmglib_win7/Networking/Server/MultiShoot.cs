﻿using System;
using System.Drawing;
using System.IO;

namespace rotmglib.Packets
{
    internal class MultiShoot : ServerPacket
    {
        public byte BulletID;
        public Int32 OwnerID;
        public byte BulletType;
        public PointF Position;
        public float Angle;
        public Int16 Damage;
        public byte NumShots;
        public float AngleIncrement;

        public override EPacketID PacketID { get { return EPacketID.MULTISHOOT; } }

        public override void Deserialize(DReader dr)
        {
            BulletID = dr.ReadByte();
            OwnerID = dr.ReadInt32();
            BulletType = dr.ReadByte();
            Position = dr.ReadPoint();
            Angle = dr.ReadSingle();
            Damage = dr.ReadInt16();
            NumShots = dr.ReadByte();
            AngleIncrement = dr.ReadSingle();
        }
    }
}