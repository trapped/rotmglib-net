﻿using System.IO;

namespace rotmglib.Packets
{
    internal class TradeChanged : ServerPacket
    {
        public bool[] MarkedItems;

        public override EPacketID PacketID { get { return EPacketID.TRADECHANGED; } }

        public override void Deserialize(DReader dr)
        {
            MarkedItems = dr.ReadBools();
        }
    }
}