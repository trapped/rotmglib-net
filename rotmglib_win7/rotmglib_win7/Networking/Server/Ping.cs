﻿using System;
using System.IO;

namespace rotmglib.Packets
{
    internal class Ping : ServerPacket
    {
        public Int32 Serial;

        public override EPacketID PacketID { get { return EPacketID.PING; } }

        public override void Deserialize(DReader dr)
        {
            Serial = dr.ReadInt32();
        }
    }
}