﻿using System;
using System.Drawing;
using System.IO;

namespace rotmglib.Packets
{
    internal class Goto : ServerPacket
    {
        public Int32 ObjectID;
        public PointF Position;

        public override EPacketID PacketID { get { return EPacketID.GOTO; } }

        public override void Deserialize(DReader dr)
        {
            ObjectID = dr.ReadInt32();
            Position = dr.ReadPoint();
        }
    }
}