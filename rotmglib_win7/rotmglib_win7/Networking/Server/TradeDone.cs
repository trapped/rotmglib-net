﻿using System;
using System.IO;

namespace rotmglib.Packets
{
    internal class TradeDone : ServerPacket
    {
        public Int32 Result;
        public string Description;

        public override EPacketID PacketID { get { return EPacketID.TRADEDONE; } }

        public override void Deserialize(DReader dr)
        {
            Result = dr.ReadInt32();
            Description = dr.ReadString();
        }
    }
}