﻿using System;
using System.IO;

namespace rotmglib.Packets
{
    internal class InventoryResult : ServerPacket
    {
        public Int32 Result;

        public override EPacketID PacketID { get { return EPacketID.INVRESULT; } }

        public override void Deserialize(DReader dr)
        {
            Result = dr.ReadInt32();
        }
    }
}