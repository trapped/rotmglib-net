﻿using System;
using System.IO;

namespace rotmglib.Packets
{
    internal class Failure : ServerPacket
    {
        public Int32 ErrorID;
        public string ErrorMessage;

        public override EPacketID PacketID { get { return EPacketID.FAILURE; } }

        public override void Deserialize(DReader dr)
        {
            ErrorID = dr.ReadInt32();
            ErrorMessage = dr.ReadString();
        }
    }
}