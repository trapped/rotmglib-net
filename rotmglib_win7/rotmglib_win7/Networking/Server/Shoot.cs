﻿using System;
using System.Drawing;
using System.IO;

namespace rotmglib.Packets
{
    internal class Shoot : ServerPacket
    {
        public byte BulletID;
        public Int32 OwnerID;
        public Int32 ContainerType;
        public PointF Position;
        public float Angle;
        public Int16 Damage;

        public override EPacketID PacketID { get { return EPacketID.SHOOT; } }

        public override void Deserialize(DReader dr)
        {
            BulletID = dr.ReadByte();
            OwnerID = dr.ReadInt32();
            ContainerType = dr.ReadInt32();
            Position = dr.ReadPoint();
            Angle = dr.ReadSingle();
            Damage = dr.ReadInt16();
        }
    }
}