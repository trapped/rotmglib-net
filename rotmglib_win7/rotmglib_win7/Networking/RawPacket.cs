﻿namespace rotmglib
{
    public class RawPacket : Packet
    {
        public override EPacketID PacketID { get { return packetID; } }

        private EPacketID packetID;
        private byte[] payload;

        virtual public byte[] GetPayload()
        {
            return payload;
        }

        public RawPacket(EPacketID ID, byte[] payload)
        {
            this.packetID = ID;
            this.payload = payload;
        }
    }
}