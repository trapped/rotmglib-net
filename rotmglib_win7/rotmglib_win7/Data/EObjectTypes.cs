﻿namespace rotmglib
{
    internal enum EObjectType : short
    {
        KNIGHT = 0x031E,
        NECROMANCER = 0x0321,
        VAULT_CHEST = 0x0504,
        LOCKED_VAULT_CHEST = 0x0505,
        VAULT_PORTAL = 0x0720
    }
}