﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace rotmglib
{
    public class DWriter : BinaryWriter
    {
        public DWriter(Stream s)
            : base(s, Encoding.UTF8)
        {
        }

        public override void Write(short value)
        {
            base.Write(IPAddress.HostToNetworkOrder(value));
        }

        public override void Write(int value)
        {
            base.Write(IPAddress.HostToNetworkOrder(value));
        }

        public override void Write(long value)
        {
            base.Write(IPAddress.HostToNetworkOrder(value));
        }

        public override void Write(ushort value)
        {
            base.Write((ushort)IPAddress.HostToNetworkOrder((short)value));
        }

        public override void Write(uint value)
        {
            base.Write((uint)IPAddress.HostToNetworkOrder((int)value));
        }

        public override void Write(ulong value)
        {
            base.Write((ulong)IPAddress.HostToNetworkOrder((long)value));
        }

        public override void Write(float value)
        {
            Write (BitConverter.ToInt32(BitConverter.GetBytes(value), 0));
        }

        public override void Write(double value)
        {
            Write(BitConverter.DoubleToInt64Bits(value));
        }

        public void WriteNullTerminatedString(string str)
        {
            Write(Encoding.UTF8.GetBytes(str));
            Write((byte)0);
        }

        public void WriteUTF(string str)
        {
            if (str == null)
                Write((short)0);
            else
            {
                Write((short)str.Length);
                Write(Encoding.UTF8.GetBytes(str));
            }
        }

        public void Write32UTF(string str)
        {
            Write((int)str.Length);
            Write(Encoding.UTF8.GetBytes(str));
        }

        public void WriteVector<T>(IEnumerable<T> value) where T : ISerializable
        {
            if (value == null)
            {
                Write((Int16)0);
                return;
            }
            Write((Int16)value.Count());
            foreach (var i in value)
                i.Serialize(this);
        }

        public void Write(PointF point)
        {
            Write(point.X);
            Write(point.Y);
        }

        public void Write(bool[] value)
        {
            Write((Int16)value.Length);
            foreach (var i in value)
            {
                Write(i);
            }
        }

        public override void Write(string value)
        {
            WriteUTF(value);
        }
    }
}