﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Drawing;
using System.Collections.Generic;


namespace rotmglib
{

    public class DReader : BinaryReader
    {
        public DReader(Stream s)
            : base(s, Encoding.UTF8)
        {
        }

        public override short ReadInt16()
        {
            return IPAddress.NetworkToHostOrder(base.ReadInt16());
        }

        public override int ReadInt32()
        {
            return IPAddress.NetworkToHostOrder(base.ReadInt32());
        }

        public override long ReadInt64()
        {
            return IPAddress.NetworkToHostOrder(base.ReadInt64());
        }

        public override ushort ReadUInt16()
        {
            return (ushort)IPAddress.NetworkToHostOrder((short)base.ReadUInt16());
        }

        public override uint ReadUInt32()
        {
            return (uint)IPAddress.NetworkToHostOrder((int)base.ReadUInt32());
        }

        public override ulong ReadUInt64()
        {
            return (ulong)IPAddress.NetworkToHostOrder((long)base.ReadUInt64());
        }

        public override float ReadSingle()
        {
            return BitConverter.ToSingle(
                BitConverter.GetBytes(ReadUInt32()),
                0
            );
        }

        public override double ReadDouble()
        {
            return BitConverter.Int64BitsToDouble(ReadInt64());
        }

        public string ReadNullTerminatedString()
        {
            StringBuilder ret = new StringBuilder();
            byte b = ReadByte();
            while (b != 0)
            {
                ret.Append((char)b);
                b = ReadByte();
            }
            return ret.ToString();
        }

        public string ReadUTF()
        {
            return Encoding.UTF8.GetString(ReadBytes(ReadInt16()));
        }

        public string Read32UTF()
        {
            return Encoding.UTF8.GetString(ReadBytes(ReadInt32()));
        }

        public T[] ReadVector<T>() where T : ISerializable, new()
        {
            var len = ReadInt16();
            T[] data = new T[len];

            for (int i = 0; i < len; i++)
            {
                data[i] = new T();
                data[i].Deserialize(this);
            }

            return data;
        }

        public List<T> ReadList<T>() where T : ISerializable, new()
        {
            List<T> result = new List<T>();
            var len = ReadInt16();

            for (int i = 0; i < len; i++)
            {
                T obj = new T();
                obj.Deserialize(this);
                result.Add(obj);
            }

            return result;
        }

        public PointF ReadPoint()
        {
            return new PointF(ReadSingle(), ReadSingle());
        }

        public bool[] ReadBools()
        {
            List<bool> data = new List<bool>();
            int len = ReadInt16();
            for (int i = 0; i < len; i++)
            {
                data.Add(ReadBoolean());
            }
            return data.ToArray();
        }

        public override string ReadString()
        {
            return ReadUTF();
        }
    }
}